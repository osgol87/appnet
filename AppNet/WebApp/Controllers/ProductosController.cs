﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class ProductosController : Controller
    {
        public ActionResult Index()
        {
            if (Session["usuario"] != null)
            {
                return View();
            }

            return RedirectToAction("Index", "Login");
        }
    }
}
