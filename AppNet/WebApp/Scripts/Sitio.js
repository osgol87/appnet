﻿var inProgress = false;
var sitioWeb = "http://localhost:15423";

$(document).ready(function () {
    hideProgress();
});

function showProgress() {
    $('div[id="procesando"]').show();
}

function hideProgress() {
    $('div[id="procesando"]').hide();
}

function loginUser() {
    var usuario = {
        "UserName": $('input[id="userName"]').val(),
        "Password": $('input[id="password"').val()
    };

    return $.ajax({
        type: 'POST',
        url: sitioWeb + "/api/login",
        data: usuario,
        beforeSend: function () {
            showProgress();
        },
        success: function (data) {
            console.log(data);

            if (data != null) {
                $('#loginForm').submit();
                return;
            }

            alert("El usuario no cuenta con los permisos suficientes para acceder.");
        },
        error: function (response) {
            console.log("Ha ocurrido un error");
            console.log(response.responseJSON.ExceptionMessage);
            alert(response.responseJSON.ExceptionMessage);
        }
    })
    .always(function () {
        hideProgress();
    });
}

function consultaProductos() {
    var usuario = {
        "UserName": $('input[id="userName"]').val(),
        "Password": $('input[id="password"').val()
    };

    return $.ajax({
        type: 'GET',
        url: sitioWeb + "/api/productos",
        beforeSend: function () {
            showProgress();
        },
        success: function (data) {
            console.log(data);
            var trhead = $("<tr></tr>");

            trhead.append('<th scope="col">ID</th>');
            trhead.append('<th scope="col">Nombre</th>');
            trhead.append('<th scope="col">SKU</th>');
            trhead.append('<th scope="col">Descripci&oacute;n</th>');

            var thead = $("<thead></thead>");
            thead.append(trhead);

            var table = $('<table class="table"></table>');
            table.append(thead);

            var tbody = $("<tbody></tbody>");

            if (data != null) {

                data.forEach(function (element) {
                    var tr = $("<tr></tr>");

                    tr.append('<th scope="row">' + element.Id + '</th>');
                    tr.append('<td>' + element.Nombre + '</td>');
                    tr.append('<td>' + element.Sku + '</td>');
                    tr.append('<td>' + element.Descripcion + '</td>');

                    tbody.append(tr);
                });
            }
            else {
                tbody.append('<tr><th scope="row" colspan="4" class="text-center">No se obtuvo informaci&oacute;n</th></tr>');
            }

            table.append(tbody);
            $('#lista_productos').append(table);
        },
        error: function (response) {
            console.log("Ha ocurrido un error");
            console.log(response.responseJSON.ExceptionMessage);
            alert(response.responseJSON.ExceptionMessage);
        }
    })
    .always(function () {
        hideProgress();
    });
}