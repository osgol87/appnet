﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Webapi.Models;
using System.Web.Http;
using System.Threading.Tasks;

namespace Webapi.Controllers
{
    public class LoginController : ApiController
    {
        private readonly WebapiDbContextDataContext dc;

        public LoginController()
        {
            this.dc = new WebapiDbContextDataContext();
        }

        public LoginController(WebapiDbContextDataContext dc)
        {
            this.dc = dc;
        }

        // POST api/login
        public Sesion Post([FromBody]Sesion sesion)
        {
            try
            {
                var result = this.dc.Users.FirstOrDefault(o => o.user1.Equals(sesion.UserName) && o.pwd.Equals(sesion.Password));

                if (result != null)
                {
                    return new Sesion
                    {
                        Id = result.id,
                        UserName = result.user1,
                        Password = result.pwd
                    };
                }

                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
