﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Webapi.Models;

namespace Webapi.Controllers
{
    public class ProductosController : ApiController
    {
        private readonly WebapiDbContextDataContext dc;

        public ProductosController()
        {
            this.dc = new WebapiDbContextDataContext();
        }

        public ProductosController(WebapiDbContextDataContext dc)
        {
            this.dc = dc;
        }

        // GET api/products
        public IEnumerable<Producto> Get()
        {
            try
            {
                return this.dc.Products.Select(o => new Producto
                    {
                        Id = o.id,
                        Nombre = o.Name,
                        Sku = o.Sku,
                        Descripcion = o.Description
                    }).ToList();
            }
            catch (Exception)
            {
                //return new List<Producto>
                //{
                //    new Producto
                //    {
                //        Id= 1,
                //        Nombre = "Producto 1",
                //        Sku = "0001",
                //        Descripcion = "Uno"
                //    },
                //    new Producto
                //    {
                //        Id= 2,
                //        Nombre = "Producto 2",
                //        Sku = "0002",
                //        Descripcion = "Dos"
                //    }
                //};

                return null;
            }
        }
    }
}
